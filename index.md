---
title: Konrad's Website
---

<a href="https://get.chat" target="_blank">get.chat</a> - Co-Founder

<a href="https://thiefguild.com" target="_blank">thiefguild.com</a> - Creator, Owner

<a href="https://boredfoundersclub.com" target="_blank">boredfoundersclub.com</a> - Bored Founder

---

Contact me by my first name **@kondz.io**.

---

<small>

[GitLab](https://gitlab.com/ktalik)
[GitHub](https://github.com/ktalik)
[LinkedIn](https://www.linkedin.com/in/ktalik/)
[BlueSky](https://bsky.app/profile/konradtalik.bsky.social)
[WhatsApp](https://wa.me/48512420042) (only text messages)

</small>
